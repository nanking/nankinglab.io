---
layout: post
title:  "最好用的python 日志模块 loguru"
date:   2019-10-25 10:59:10 +0700
categories: ["python"]
tags: ["python"]
draft: false
---
# 最好用的python 日志模块 loguru
代码片段如下

```python {.line-numbers}
import sys
import os
from datetime import datetime
from loguru import logger as log


class Singleton(type):
    """
    单例模式
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Logger(metaclass=Singleton):
    def __init__(self,logpath=None):
        self.logpath = logpath

    @property
    def logger(self):
        timelog = datetime.now().strftime("%Y%m%d")
        logfilename = "UIAuto_{timelog}.log".format(timelog=timelog)
        stdOutHandler = {"sink": sys.stdout,
                         "level": "INFO",
                         "backtrace": True,
                         "colorize": True,
                         "format": "<green>{time:YYYY-MM-DD HH:mm:ss.SSS}</green> | <level>{level: <8}</level> | "
                                   "<cyan>{file}:{function}:{module}:{name}:{line} </cyan> - <level>{message}</level>"
                         }
        if self.logpath:
            fileHandler = {"sink": os.path.join(self.logpath, logfilename),
                           "backtrace": True,
                           "level": "INFO",
                           "rotation": "00:00",
                           "format": "{time:YYYY-MM-DD HH:mm:ss} | {level} |{file}:{function}:{module}:{name}:{line} {message}",
                           "encoding": "UTF-8",
                           }
        loggerconfig = {"handlers": [fileHandler, stdOutHandler]} if self.logpath else {
            "handlers": [stdOutHandler]}
        log.configure(**loggerconfig)
        return log

    @logger.setter
    def logger(self, loggerins):
        self.logger = loggerins

logger= Logger().logger
```