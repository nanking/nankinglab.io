---
title: RNN原理（pytorch)
date: 2019-12-10 20:06:14 +0800
categories: python
---
## 1. RNN
$$h_{t}=f_{W}\left(h_{t-1}, x_{t}\right)$$

$$ h_{t}=\tanh \left(W_{h h} h_{t-1}+W_{x h} x_{t}\right) $$ 

$$ h_{t}=\tanh \left(W_{xh} x_{t}+b_{xh}+W_{hh} h_{t-1}+b_{hh}\right)
$$

$$  y_{t}=W_{h y} h_{t} $$

当前时间 $t$ 输入门特征点feature: $x_t$

的输入维度为

$x_t$ : [batch,feature\_length]

对应的输入门权重矩阵$W_{xh}$维度为

$W_{wt}$ : [hideen\_length,feature\_length]

上一层的输出记忆单元$h_{t-1}$的维度为

$h_{t-1}$ : [batch,hideen\_length]

对应的权重矩$W_{h,h}$的维度为

$W_{hh}$ : [hidden\_length,hideen\_length]

所以当前层的记忆单元输出$h_t$为

$$h_{t} =\tanh \left(W_{h h} h_{t-1}+W_{x h} x_{t}\right) $$

其中

$$W_{xh}x_t+W_{hh}h_{t-1}=$$

[batch,feature\_length]@[hidden\_length,feature\_length]$^T$
+[batch,hidden_lenght]@[hidden\_length,hidden_length]$^T$

## 2. RNN 的输入输出

### 2.1  输入 input, h_0

- `input`  输入序列的特征，如果 batch first 则 shape 为 (batch，seq_len, input_size) 反之 为(seq_len, batch, input_size) ，其中 seq_len 为序列长度，input_size 为特征大小。

> input of shape (seq_len, batch, input_size): tensor containing the features of the input sequence. The input can also be a packed variable length sequence. See  [`torch.nn.utils.rnn.pack_padded_sequence()`](https://pytorch.org/docs/stable/nn.html?highlight=rnn#torch.nn.utils.rnn.pack_padded_sequence) or `[torch.nn.utils.rnn.pack_sequence()](https://pytorch.org/docs/stable/nn.html?highlight=rnn#torch.nn.utils.rnn.pack_sequence)` for details.

- `h_0`  为初始记忆单元，即batch中每个元素（时间序列点）的隐含状态，默认状态为0

> h_0 of shape (num_layers * num_directions, batch, hidden_size): tensor containing the initial hidden state for each element in the batch. Defaults to zero if not provided. If the RNN is bidirectional, num_directions should be 2, else it should be 1.

### 2.2 输出 output, h_n

- output ($h_{all}$) ： RNN最后一层中每个元素（时间序列中的每个时间点）隐含的隐含状态，其shape为(seq_len, batch, num_directions * hidden_size)

> output of shape `(seq_len, batch, num_directions * hidden_size)`: tensor containing the output features **(h_t)** from the last layer of the RNN, for each **t**. If a torch.nn.utils.rnn.PackedSequence has been given as the input, the output will also be a packed sequence.
For the unpacked case, the directions can be separated using `output.view(seq_len, batch, num_directions, hidden_size)`, with forward and backward being direction 0 and 1 respectively. Similarly, the directions can be separated in the packed case.

- h_n： 时间序列最后一个时间点对应的所有layer的隐含状态。

> **h_n of shape `(num_layers * num_directions, batch, hidden_size)`: tensor containing the hidden state for t = seq_len.Like *output*, the layers can be separated using `h_n.view(num_layers, num_directions, batch, hidden_size)`.**

### 2.3. RNN 输入输出Shape

**N : batch size**

- input1 ：

$$(L,N,Hin​)$$

**tensor containing input features where**

$$H_{in}=\text{input\_size}$$

**and L represents a sequence length.**

- **Input2 (h_0):**

$$(S, N, H_{out})$$

tensor containing the initial hidden state for each element in the batch

$$H_{out}=\text{hidden\_size}$$

where

$$ S=\text{num\_layers} * \text{num\_directions}$$

If the RNN is bidirectional, num_directions should be 2, else it should be 1

- **Output1  (h_all):**

$$(L, N, H_{all})$$

**where**

$$H_{all}=\text{num\_directions} * \text{hidden\_size}$$

- **Output2 (h_n):**

$$(S, N, H_{out})$$

> tensor containing the next hidden state for each element in the batch

## 3. RNN 参数

- 输入权重 **~RNN.weight_ih_l[k]** – the learnable input-hidden weights of the k-th layer, of shape `(hidden_size, input_size)` for k = 0. Otherwise, the shape is (hidden_size, num_directions * hidden_size)
- 记忆权重 **~RNN.weight_hh_l[k]** – the learnable hidden-hidden weights of the k-th layer, of shape `(hidden_size, hidden_size)`
- 输入偏置 **~RNN.bias_ih_l[k]** – the learnable input-hidden bias of the k-th layer, of shape `(hidden_size)`
- 记忆偏置 **~RNN.bias_hh_l[k]** – the learnable hidden-hidden bias of the k-th layer, of shape `(hidden_size)`

## Example (Pytorch)
- 构造序列长度为 5 ，batch_size = 3 ,特征输入维度为10的序列数据 $X$
- hidden_size = 10
- num_layers = 1

```python
rnn = nn.RNN(input_size=10, hidden_size=20, num_layers=1, batch_first=False)
X = torch.randn(size=(5, 3, 10))
h_init = torch.randn(1, 3, 20)
h_all, h_n = rnn(input=X, hx=h_init)

print(rnn.weight_ih_l0.shape)
print(rnn.bias_ih_l0.shape)
print(rnn.weight_hh_l0.shape)
print(rnn.bias_hh_l0.shape)
```
- shape 
```
torch.Size([20, 10])
torch.Size([20])
torch.Size([20, 20])
torch.Size([20])
```
输入权重矩阵$W_{ih}$的shape 为 [20, 10]
记忆权重矩阵$W_{hh}$的shape 为 [20, 20] 
## 其他
![image3](https://i.postimg.cc/QtMFpJ0j/image.png)