---
layout: post
title:  "Pytorch 正则化/动量因子"
date:   2019-05-12 17:34:10 +0700
tags: ["python", "pytorch"]
categories: ["深度学习"]
draft: false
---
## 正则化
$$
J(\theta)=-\frac{1}{m} \sum_{i=1}^{m}\left[y_{i} \ln \hat{y}_{i}+\left(1-y_{i}\right) \ln \left(1-\hat{y}_{i}\right)\right]
$$

### L2-regularization
```python
device = torch.device('cuda:0')
net = MLP().to(device)
optimizer = optim.SGD(net.parameters(),
					  lr=learning_rate,
					  weight_decay=0.01)
criteon = nn.CrossEntropyLoss().to(device)
```

### L1-regularization
pytorch中L1范数没有实现，需要我们自己实现
```python
regularization_loss = 0
weight_decay = 0.01
for param in model.parameters()
	regularization_loss += torch.sum(torch.abs(param))
classify_loss = criteen(logits,target)
loss = classify_loss + weight_decay * regularization_loss
optimizer.zero_gard()
loss.backward()
optimizer.step()
```
## 动量因子
$$
\begin{aligned} w^{k+1} &=w^{k}-\alpha \nabla f\left(w^{k}\right) \\ z^{k+1} &=\beta z^{k}+\nabla f\left(w^{k}\right) \\ w^{k+1} &=w^{k}-\alpha z^{k+1} \end{aligned}
$$



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTg0NzQ4ODM2MSwtMjE0MDU5NTQwNywtMT
Q3OTk3ODE5N119
-->