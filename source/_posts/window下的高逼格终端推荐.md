---
title: "windows下高逼格终端推荐"
date: 2019-10-07 16:31:30
categories: ["cs"]
tags: ["cs"]
draft: false
---

# Terminus
官方网址 [https://www.termius.com/](https://www.termius.com/)

# Hyper.js
官方网址 [https://hyper.is/](https://hyper.is/)
为了能支持 [powerlevel10k](https://github.com/romkatv/powerlevel10k) zsh 主题，需要如下配置设置字体为 `MesloLGS NF Regular` 再`p10k configure`进行主题配置。
```
fontFamily: '"MesloLGS NF Regular", "Fira Code", monospace',
```
如果需要设置默认终端为Ubuntu 18.04 并设置默认路径为用户根目录`~`

- 设置终端
 ```  shell: 'C:\\Windows\\System32\\wsl.exe',```
- 设置用户目录
```  shellArgs: ['~'], ```
